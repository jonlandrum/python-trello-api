class Board:
    def __init__(self):
        self._id = None
        self._name = ""
        self._desc = ""
        self._descData = None
        self._closed = False
        self._idOrganization = ""
        self._pinned = False
        self._url = ""
        self._shortUrl = ""
        self._prefs = {}
        self._labelNames = {}
        self._starred = False
        self._limits = {}
        self._memberships = []

    def get_id(self):
        return self._id

    def get_name(self):
        return self._name

    def get_desc(self):
        return self._desc

    def get_descData(self):
        return self._descData

    def get_closed(self):
        return self._closed

    def get_idOrganization(self):
        return self._idOrganization

    def get_pinned(self):
        return self._pinned

    def get_url(self):
        return self._url

    def get_shortUrl(self):
        return self._shortUrl

    def get_prefs(self):
        return self._prefs

    def get_labelNames(self):
        return self._labelNames

    def get_starred(self):
        return self._starred

    def get_limits(self):
        return self._limits

    def get_memberships(self):
        return self._memberships

    def set_id(self, id):
        self._id = id

    def set_name(self, name):
        self._name = name

    def set_desc(self, desc):
        self._desc = desc

    def set_descData(self, descData):
        self._descData = descData

    def set_closed(self, closed):
        self._closed = closed

    def set_idOrganization(self, idOrganization):
        self._idOrganization = idOrganization

    def set_pinned(self, pinned):
        self._pinned = pinned

    def set_url(self, url):
        self._url = url

    def set_shortUrl(self, shortUrl):
        self._shortUrl = shortUrl

    def set_prefs(self, prefs):
        self._prefs = prefs

    def set_labelNames(self, labelNames):
        self._labelNames = labelNames

    def set_starred(self, starred):
        self._starred = starred

    def set_limits(self, limits):
        self._limits = limits

    def set_memberships(self, memberships):
        self._memberships = memberships
