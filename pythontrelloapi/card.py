import datetime


class Card:
    def __init__(self, idBoard, idList, name):
        self._id = None
        self._badges = {
            "votes": 0,
            "viewingMemberVoted": False,
            "subscribed": False,
            "fogbugz": "",
            "checkItems": 0,
            "checkItemsChecked": 0,
            "comments": 0,
            "attachments": 0,
            "description": False,
            "due": None,
            "dueComplete": False
        }
        self._checkItemStates = []
        self._closed = False
        self._dateLastActivity = datetime.datetime.utcnow().isoformat()
        self._desc = ""
        self._descData = {}
        self._due = None
        self._dueComplete = False
        self._idAttachmentCover = None
        self._idBoard = idBoard
        self._idChecklists = []
        self._idLabels = []
        self._idList = idList
        self._idMembers = []
        self._idMembersVoted = []
        self._idShort = None
        self._labels = []
        self._manualCoverAttachment = False
        self._name = name
        self._pos = 0.0
        self._shortLink = ""
        self._shortUrl = ""
        self._subscribed = False
        self._url = ""

    def get_id(self):
        return self._id

    def get_badges(self):
        return self._badges

    def get_checkItemStates(self):
        return self._checkItemStates

    def get_closed(self):
        return self._closed

    def get_dateLastActivity(self):
        return self._dateLastActivity

    def get_desc(self):
        return self._desc

    def get_descData(self):
        return self._descData

    def get_due(self):
        return self._due

    def get_dueComplete(self):
        return self._dueComplete

    def get_idAttachmentCover(self):
        return self._idAttachmentCover

    def get_idBoard(self):
        return self._idBoard

    def get_idChecklists(self):
        return self._idChecklists

    def get_idLabels(self):
        return self._idLabels

    def get_idList(self):
        return self._idList

    def get_idMembers(self):
        return self._idMembers

    def get_idMembersVoted(self):
        return self._idMembersVoted

    def get_idShort(self):
        return self._idShort

    def get_labels(self):
        return self._labels

    def get_manualCoverAttachment(self):
        return self._manualCoverAttachment

    def get_name(self):
        return self._name

    def get_pos(self):
        return self._pos

    def get_shortLink(self):
        return self._shortLink

    def get_shortUrl(self):
        return self._shortUrl

    def get_subscribed(self):
        return self._subscribed

    def get_url(self):
        return self._url

    def set_id(self, id):
        self._id = id

    def set_badges(self, badges):
        self._badges = badges

    def set_checkItemStates(self, checkItemStates):
        self._checkItemStates = checkItemStates

    def set_closed(self, closed):
        self._closed = closed

    def set_dateLastActivity(self, dateLastActivity):
        self._dateLastActivity = dateLastActivity

    def set_desc(self, desc):
        self._desc = desc

    def set_descData(self, descData):
        self._descData = descData

    def set_due(self, due):
        self._due = due

    def set_dueComplete(self, dueComplete):
        self._dueComplete = dueComplete

    def set_idAttachmentCover(self, idAttachmentCover):
        self._idAttachmentCover = idAttachmentCover

    def set_idBoard(self, idBoard):
        self._idBoard = idBoard

    def set_idChecklists(self, idChecklists):
        self._idChecklists = idChecklists

    def set_idLabels(self, idLabels):
        self._idLabels = idLabels

    def set_idList(self, idList):
        self._idList = idList

    def set_idMembers(self, idMembers):
        self._idMembers = idMembers

    def set_idMembersVoted(self, idMembersVoted):
        self._idMembersVoted = idMembersVoted

    def set_idShort(self, idShort):
        self._idShort = idShort

    def set_labels(self, labels):
        self._labels = labels

    def set_manualCoverAttachment(self, manualCoverAttachment):
        self._manualCoverAttachment = manualCoverAttachment

    def set_name(self, name):
        self._name = name

    def set_pos(self, pos):
        self._pos = pos

    def set_shortLink(self, shortLink):
        self._shortLink = shortLink

    def set_shortUrl(self, shortUrl):
        self._shortUrl = shortUrl

    def set_subscribed(self, subscribed):
        self._subscribed = subscribed

    def set_url(self, url):
        self._url = url
