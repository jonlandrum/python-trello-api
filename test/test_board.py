from pythontrelloapi import Board

import unittest


class TestBoard(unittest.TestCase):
    def setUp(self):
        self.board = Board()

    def test_empty_id(self):
        self.assertEqual(self.board.get_id(), None)

    def test_empty_name(self):
        self.assertEqual(self.board.get_name(), "")

    def test_empty_desc(self):
        self.assertEqual(self.board.get_desc(), "")

    def test_empty_descData(self):
        self.assertEqual(self.board.get_descData(), None)

    def test_empty_closed(self):
        self.assertEqual(self.board.get_closed(), False)

    def test_empty_idOrganization(self):
        self.assertEqual(self.board.get_idOrganization(), "")

    def test_empty_pinned(self):
        self.assertEqual(self.board.get_pinned(), False)

    def test_empty_url(self):
        self.assertEqual(self.board.get_url(), "")

    def test_empty_shortUrl(self):
        self.assertEqual(self.board.get_shortUrl(), "")

    def test_empty_prefs(self):
        self.assertEqual(self.board.get_prefs(), {})

    def test_empty_labelNames(self):
        self.assertEqual(self.board.get_labelNames(), {})

    def test_empty_starred(self):
        self.assertEqual(self.board.get_starred(), False)

    def test_empty_limits(self):
        self.assertEqual(self.board.get_limits(), {})

    def test_empty_memberships(self):
        self.assertEqual(self.board.get_memberships(), [])

    def test_id(self):
        self.board.set_id("test")
        self.assertEqual(self.board.get_id(), "test")

    def test_name(self):
        self.board.set_name("test")
        self.assertEqual(self.board.get_name(), "test")

    def test_desc(self):
        self.board.set_desc("test")
        self.assertEqual(self.board.get_desc(), "test")

    def test_descData(self):
        descData = {
            "emoji": {
                "morty": "https://trello-emoji.s3.amazonaws.com/556c8537a1928ba745504dd8/f40ea4f5ecea8443875c27986760d8b3/tumblr_nszc7944yh1uccyhso1_1280.png"
            }
        }
        self.board.set_descData(descData)
        self.assertEqual(self.board.get_descData(), descData)

    def test_closed(self):
        self.board.set_closed(True)
        self.assertEqual(self.board.get_closed(), True)

    def test_idOrganization(self):
        self.board.set_idOrganization("test")
        self.assertEqual(self.board.get_idOrganization(), "test")

    def test_pinned(self):
        self.board.set_pinned(True)
        self.assertEqual(self.board.get_pinned(), True)

    def test_url(self):
        self.board.set_url("https://trello.com/b/12345678")
        self.assertEqual(self.board.get_url(), "https://trello.com/b/12345678")

    def test_shortUrl(self):
        self.board.set_shortUrl("12345678")
        self.assertEqual(self.board.get_shortUrl(), "12345678")

    def test_prefs(self):
        prefs = {
            "permissionLevel": "public",
            "voting": "disabled",
            "comments": "members"
        }
        self.board.set_prefs(prefs)
        self.assertEqual(self.board.get_prefs(), prefs)

    def test_labelNames(self):
        labelNames = {
            "green": "low priority",
            "yellow": "medium priority",
            "red": "high priority"
        }
        self.board.set_labelNames(labelNames)
        self.assertEqual(self.board.get_labelNames(), labelNames)

    def test_starred(self):
        self.board.set_starred(True)
        self.assertEqual(self.board.get_starred(), True)

    def test_limits(self):
        limits = {
            "boards": {
                "totalMembersPerBoard": {
                    "status": "ok",
                    "disableAt": 1520,
                    "warnAt": 1440
                }
            }
        }
        self.board.set_limits(limits)
        self.assertEqual(self.board.get_limits(), limits)

    def test_memberships(self):
        memberships = [
            {
                "id": "5612e4fb1b25c15e8737234b",
                "idMember": "53baf533e697a982248cd73f",
                "memberType": "admin",
                "unconfirmed": False
            },
            {
                "id": "5925e4fc63096260c349cbd4",
                "idMember": "53cd82cd7ed746db278c4f32",
                "memberType": "normal",
                "unconfirmed": False
            }
        ]
        self.board.set_memberships(memberships)
        self.assertEqual(self.board.get_memberships(), memberships)


if __name__ == '__main__':
    unittest.main()
