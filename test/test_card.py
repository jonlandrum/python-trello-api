from pythontrelloapi import Card

import datetime
import unittest


class TestCard(unittest.TestCase):
    def setUp(self):
        idBoard = ""
        idList = ""
        name = ""
        self.card = Card(idBoard=idBoard, idList=idList, name=name)

    def test_empty_id(self):
        self.assertEqual(self.card.get_id(), None)

    def test_empty_badges(self):
        badges = {
            "votes": 0,
            "viewingMemberVoted": False,
            "subscribed": False,
            "fogbugz": "",
            "checkItems": 0,
            "checkItemsChecked": 0,
            "comments": 0,
            "attachments": 0,
            "description": False,
            "due": None,
            "dueComplete": False
        }
        self.assertEqual(self.card.get_badges(), badges)

    def test_empty_checkItemStates(self):
        self.assertEqual(self.card.get_checkItemStates(), [])

    def test_empty_closed(self):
        self.assertEqual(self.card.get_closed(), False)

    def test_empty_dateLastActivity_date(self):
        dateLastActivity = datetime.datetime.utcnow().isoformat()
        self.assertEqual(self.card.get_dateLastActivity().split('T')[0],
                         dateLastActivity.split('T')[0])

    def test_empty_dateLastActivity_hour(self):
        dateLastActivity = datetime.datetime.utcnow().isoformat()
        self.assertEqual(self.card.get_dateLastActivity().split('T')[1].split(':')[0],
                         dateLastActivity.split('T')[1].split(':')[0])

    def test_empty_dateLastActivity_minute(self):
        dateLastActivity = datetime.datetime.utcnow().isoformat()
        self.assertEqual(self.card.get_dateLastActivity().split('T')[1].split(':')[1],
                         dateLastActivity.split('T')[1].split(':')[1])

    # Note: seconds cannot be accurately tested here

    def test_empty_desc(self):
        self.assertEqual(self.card.get_desc(), "")

    def test_empty_descData(self):
        self.assertEqual(self.card.get_descData(), {})

    def test_empty_due(self):
        self.assertEqual(self.card.get_due(), None)

    def test_empty_dueComplete(self):
        self.assertEqual(self.card.get_dueComplete(), False)

    def test_empty_idAttachmentCover(self):
        self.assertEqual(self.card.get_idAttachmentCover(), None)

    def test_empty_idBoard(self):
        self.assertEqual(self.card.get_idBoard(), "")

    def test_empty_idChecklists(self):
        self.assertEqual(self.card.get_idChecklists(), [])

    def test_empty_idLabels(self):
        self.assertEqual(self.card.get_idLabels(), [])

    def test_empty_idMembers(self):
        self.assertEqual(self.card.get_idMembers(), [])

    def test_empty_idMembersVoted(self):
        self.assertEqual(self.card.get_idMembersVoted(), [])

    def test_empty_idShort(self):
        self.assertEqual(self.card.get_idShort(), None)

    def test_empty_labels(self):
        self.assertEqual(self.card.get_labels(), [])

    def test_empty_manualCoverAttachment(self):
        self.assertEqual(self.card.get_manualCoverAttachment(), False)

    def test_empty_name(self):
        self.assertEqual(self.card.get_name(), "")

    def test_empty_pos(self):
        self.assertEqual(self.card.get_pos(), 0.0)

    def test_empty_shortLink(self):
        self.assertEqual(self.card.get_shortLink(), "")

    def test_empty_shortUrl(self):
        self.assertEqual(self.card.get_shortUrl(), "")

    def test_empty_subscribed(self):
        self.assertEqual(self.card.get_subscribed(), False)

    def test_empty_url(self):
        self.assertEqual(self.card.get_url(), "")

    def test_id(self):
        self.card.set_id("test")
        self.assertEqual(self.card.get_id(), "test")

    def test_badges(self):
        badges = {
            "votes": 1,
            "viewingMemberVoted": True,
            "subscribed": True,
            "fogbugz": "test",
            "checkItems": 1,
            "checkItemsChecked": 1,
            "comments": 1,
            "attachments": 1,
            "description": True,
            "due": datetime.datetime.utcnow().isoformat(),
            "dueComplete": True
        }
        self.card.set_badges(badges)
        self.assertEqual(self.card.get_badges(), badges)

    def test_checkItemStates(self):
        checkItemStates = [
            {
                "idCheckItem": "12345",
                "state": "complete"
            }
        ]
        self.card.set_checkItemStates(checkItemStates)
        self.assertEqual(self.card.get_checkItemStates(), checkItemStates)

    def test_closed(self):
        self.card.set_closed(True)
        self.assertEqual(self.card.get_closed(), True)

    def test_dateLastActivity(self):
        self.card.set_dateLastActivity("2017-04-07T21:26:00.365Z")
        self.assertEqual(self.card.get_dateLastActivity(), "2017-04-07T21:26:00.365Z")

    def test_desc(self):
        self.card.set_desc("test")
        self.assertEqual(self.card.get_desc(), "test")

    def test_descData(self):
        descData = {
            "emoji": {
                "morty": "https://trello-emoji.s3.amazonaws.com/556c8537a1928ba745504dd8/f40ea4f5ecea8443875c27986760d8b3/tumblr_nszc7944yh1uccyhso1_1280.png"
            }
        }
        self.card.set_descData(descData)
        self.assertEqual(self.card.get_descData(), descData)

    def test_due(self):
        self.card.set_due("2017-04-07T21:26:00.365Z")
        self.assertEqual(self.card.get_due(), "2017-04-07T21:26:00.365Z")

    def test_dueComplete(self):
        self.card.set_dueComplete(True)
        self.assertEqual(self.card.get_dueComplete(), True)

    def test_idAttachmentCover(self):
        self.card.set_idAttachmentCover("test")
        self.assertEqual(self.card.get_idAttachmentCover(), "test")

    def test_idBoard(self):
        self.card.set_idBoard("test")
        self.assertEqual(self.card.get_idBoard(), "test")

    def test_idChecklists(self):
        idChecklists = ["one", "two", "three"]
        self.card.set_idChecklists(idChecklists)
        self.assertEqual(self.card.get_idChecklists(), idChecklists)

    def test_idLabels(self):
        idLabels = ["one", "two", "three"]
        self.card.set_idLabels(idLabels)
        self.assertEqual(self.card.get_idLabels(), idLabels)

    def test_idList(self):
        self.card.set_idList("test")
        self.assertEqual(self.card.get_idList(), "test")

    def test_idMembers(self):
        idMembers = ["one", "two", "three"]
        self.card.set_idMembers(idMembers)
        self.assertEqual(self.card.get_idMembers(), idMembers)

    def test_idMembersVoted(self):
        idMembersVoted = ["one", "two", "three"]
        self.card.set_idMembersVoted(idMembersVoted)
        self.assertEqual(self.card.get_idMembersVoted(), idMembersVoted)

    def test_idShort(self):
        self.card.set_idShort(12345)
        self.assertEqual(self.card.get_idShort(), 12345)

    def test_labels(self):
        labels = ["one", "two", "three"]
        self.card.set_labels(labels)
        self.assertEqual(self.card.get_labels(), labels)

    def test_manualCoverAttachment(self):
        self.card.set_manualCoverAttachment(True)
        self.assertEqual(self.card.get_manualCoverAttachment(), True)

    def test_name(self):
        self.card.set_name("test")
        self.assertEqual(self.card.get_name(), "test")

    def test_pos(self):
        self.card.set_pos(6)
        self.assertEqual(self.card.get_pos(), 6)

    def test_shortLink(self):
        self.card.set_shortLink("12345678")
        self.assertEqual(self.card.get_shortLink(), "12345678")

    def test_shortUrl(self):
        shortUrl = "https://trello.com/c/12345678"
        self.card.set_shortUrl(shortUrl)
        self.assertEqual(self.card.get_shortUrl(), shortUrl)

    def test_subscribed(self):
        self.card.set_subscribed(True)
        self.assertEqual(self.card.get_subscribed(), True)

    def test_url(self):
        url = "https://trello.com/c/12345678/test"
        self.card.set_url(url)
        self.assertEqual(self.card.get_url(), url)


if __name__ == '__main__':
    unittest.main()
