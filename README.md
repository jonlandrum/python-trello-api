# <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Python Trello API</span>

Currently, this API only provides managing <a href="https://trello.com">Trello</a> cards and boards. Other components of the Trello API will be added later. The software in this API is based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://developers.trello.com/v1.0/reference" rel="dct:source">https://developers.trello.com/v1.0/reference</a>.

## License and Copyright

<a rel="license" href="https://www.gnu.org/licenses/agpl.txt" title="GNU AGPL v3.0"><img alt="GNU LGPLv3 License" style="border-width:0" src="https://www.gnu.org/graphics/agplv3-88x31.png" /></a>

This API is Copyright &copy; 2018, <a href="http://jonlandrum.com/" title="jonlandrum.com">Jonathan E. Landrum</a>.

This API is free software: you can redistribute it and/or modify it under the terms of the <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.gnu.org/licenses/agpl-3.0.html" rel="cc:morePermissions license" title="GNU AGPL v3.0">GNU Affero General Public License</a> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This API is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

A copy of the GNU Affero General Public License is included with this API in the file named <a rel="license" href="LICENSE" title="GNU AGPL v3.0">LICENSE</a>.

---

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

The documentation contained in this API is Copyright &copy; 2018, <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/jonlandrum/python-trello-api/" property="cc:attributionName" rel="cc:attributionURL">Jonathan E Landrum</a>.
 
It is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. This means that you are free to:

* *Share* &ndash; copy and redistribute the material in any medium or format
* *Adapt* &ndash; remix, transform, and build upon the material

for any purpose, even commercially.

The licensor cannot revoke these freedoms as long as you follow these terms:

* *Attribution* &ndash; You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* *ShareAlike* &ndash; If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

There are no additional restrictions. You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

Notices:

* You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation
* No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material

A copy of the Creative Commons Attribution-ShareAlike 4.0 International License is included with this API in the file named <a rel="license" href="COPYING" title="CC-BY-SA">COPYING</a>.
