import os
from distutils.core import setup


# Utility function to print the README file.
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="Python Trello API",
    version="0.1.0",
    author="Jonathan E. Landrum",
    author_email="me@jonlandrum.com",
    url="https://gitlab.com/jonlandrum/python-trello-api/",
    description="A Python API for interacting with a Trello board",
    long_description=read("README.md"),
    license="AGPL",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Programming Language :: Python :: 3.7"
    ]
)
