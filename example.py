from pythontrelloapi import Card

import argparse
import requests


def main():
    parser = argparse.ArgumentParser(description="Demonstrates adding a card to a Trello board")
    parser.add_argument("-k", "--key", dest="key", required=True, help="Trello API key")
    parser.add_argument("-t", "--token", dest="token", required=True, help="Trello secret token")
    parser.add_argument("-b", "--board", dest="board", required=True, help="The board ID")
    parser.add_argument("-i", "--list", dest="list", required=True, help="The list to add the card to")
    parser.add_argument("-n", "--name", dest="name", required=True, help="Name of the card")
    parser.add_argument("-a", "--labels", dest="labels", required=True, nargs="+", help="Labels to add to the card")
    args = parser.parse_args()

    card = Card(idBoard=args.board, idList=args.list, name=args.name)
    card.set_idLabels(args.labels)

    url = "https://api.trello.com/1/cards"
    querystring = {
        "name": card.get_name(),
        "idList": card.get_idList(),
        "idLabels": ",".join(map(str, card.get_idLabels())),
        "keepFromSource": "all",
        "key": args.key,
        "token": args.token
    }
    response = requests.request("POST", url, params=querystring)
    print(response.text)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        exit()
